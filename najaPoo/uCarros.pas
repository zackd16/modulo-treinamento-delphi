unit uCarros;

interface

uses
  uVeiculo;

type
  TCarro = class(TVeiculo)
  public
    constructor Create; override;
  end;

implementation

{ TCarro }

constructor TCarro.Create;
begin
  inherited;
  FQuantidadeRodas := 4;
end;

end.
