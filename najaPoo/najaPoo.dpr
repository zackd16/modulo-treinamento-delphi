program najaPoo;

uses
  Vcl.Forms,
  uPrincipal in 'uPrincipal.pas' {fmPrincipal},
  uVeiculo in 'uVeiculo.pas',
  uCarros in 'uCarros.pas',
  uMotos in 'uMotos.pas',
  uAquatico in 'uAquatico.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmPrincipal, fmPrincipal);
  Application.Run;
end.
