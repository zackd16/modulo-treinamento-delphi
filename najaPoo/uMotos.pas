unit uMotos;

interface

uses
  uVeiculo;

type
  TMoto = class(TVeiculo)
  public
    constructor Create; override;
  end;

  TTriciclo = class(TVeiculo)
  public
    constructor Create; override;
  end;


implementation

{ TMoto }

constructor TMoto.Create;
begin
  inherited;
  FQuantidadeRodas := 2;
end;

{ TTriciclo }

constructor TTriciclo.Create;
begin
  inherited;
  FQuantidadeRodas := 3;
end;

end.
