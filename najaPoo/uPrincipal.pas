unit uPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,uVeiculo;

type
  TfmPrincipal = class(TForm)
    btCarro: TButton;
    mmLog: TMemo;
    btMotos: TButton;
    btBarco: TButton;
    btJetski: TButton;
    cbTriciclo: TCheckBox;
    procedure btCarroClick(Sender: TObject);
    procedure btMotosClick(Sender: TObject);
    procedure btBarcoClick(Sender: TObject);
    procedure btJetskiClick(Sender: TObject);
  private
        FVeiculo: TVeiculo;
    procedure SalvarInformacoes(aVeiculo: TVeiculo);


  public
    { Public declarations }
  end;

var
  fmPrincipal: TfmPrincipal;

implementation

{$R *.dfm}

uses
  uCarros, uMotos, uAquatico;

procedure TfmPrincipal.btBarcoClick(Sender: TObject);
begin
  FVeiculo := TBarco.Create;
  try
    try
      FVeiculo.Marca := 'Karmarine';
      FVeiculo.Ano := 2020;
      FVeiculo.Cor := 'Branco';
      FVeiculo.Motor := '6,5HP';

      SalvarInformacoes(FVeiculo);
    except
      on E: Exception do
        ShowMessage(E.Message);
    end;
  finally
    FVeiculo.Free;
  end;

end;

procedure TfmPrincipal.btCarroClick(Sender: TObject);
begin
  FVeiculo := TCarro.Create;
  try
    try
      FVeiculo.Marca := 'Fiat';
      FVeiculo.Ano := 2010;
      FVeiculo.Cor := 'Amarelo';
      FVeiculo.Motor := '1.0';
      
      SalvarInformacoes(FVeiculo);
    except
      on E: Exception do
        ShowMessage(E.Message);
    end;
  finally
    FVeiculo.Free;
  end;

end;

procedure TfmPrincipal.btJetskiClick(Sender: TObject);
begin
  FVeiculo := TJetSki.Create;
  try
    try
      FVeiculo.Marca := 'PierPlas';
      FVeiculo.Ano := 2021;
      FVeiculo.Cor := 'Branco';
      FVeiculo.Motor := '150cc';

      SalvarInformacoes(FVeiculo);
    except
      on E: Exception do
        ShowMessage(E.Message);
    end;
  finally
    FVeiculo.Free;
  end;

end;

procedure TfmPrincipal.btMotosClick(Sender: TObject);
begin
  if cbTriciclo.Checked then
   FVeiculo := TTriciclo.Create
  else
  FVeiculo := TMoto.Create;
  try
    try
      FVeiculo.Marca := 'Honda';
      FVeiculo.Ano := 2019;
      FVeiculo.Cor := 'Verde';
      FVeiculo.Motor := '150cc';
      
      SalvarInformacoes(FVeiculo);
    except
      on E: Exception do
        ShowMessage(E.Message);
    end;
  finally
    FVeiculo.Free;
  end;

end;

procedure TfmPrincipal.SalvarInformacoes(aVeiculo: TVeiculo);
begin
  mmLog.Lines.Add('-|-|-|-|-'+aVeiculo.ToString+'-|-|-|-|-');
  mmLog.Lines.Add('Marca: ' + aVeiculo.Marca);
  mmLog.Lines.Add('Ano: ' +aVeiculo.Ano.ToString);
  mmLog.Lines.Add('Cor: ' +aVeiculo.Cor);
  mmLog.Lines.Add('Numero de rodas: ' +aVeiculo.QuantidadeRodas.ToString);
  mmLog.Lines.Add('Motor: ' +aVeiculo.Motor);
end;

end.
