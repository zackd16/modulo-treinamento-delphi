unit uAquatico;

interface

uses
  uVeiculo;

type
  TBarco = class(TVeiculo)
  public
    constructor Create; override;
  end;
  TJetSki = class(TVeiculo)
  public
    constructor Create; override;
  end;

implementation

{ TBarco }

constructor TBarco.Create;
begin
  inherited;
  FQuantidadeRodas := 0;
end;

{ TJetSki }

constructor TJetSki.Create;
begin
  inherited;
  FQuantidadeRodas := 0;
end;

end.
