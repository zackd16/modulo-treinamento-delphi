unit uVeiculo;

interface

type
  TVeiculo = class(TObject)
  private
    FMarca: String;
    FAno: integer;
    FCor: String;
    FMotor: String;
    procedure SetMarca(const Value: String);
    // Acessado somente pela classe(TVeiculo)
  protected
    FTipo: String;
    FQuantidadeRodas: integer;
    // Acessado pela classe(TVeiculo) e suas heran�as

  public
    property Marca: String read FMarca write SetMarca;
    property Ano: integer read FAno write FAno;
    property Cor: String read FCor write FCor;
    property QuantidadeRodas: integer read FQuantidadeRodas;
    property Motor: String read FMotor write FMotor;

    constructor Create; Virtual; //constructor inicializar recursos na classe
                                // overlaod � o par�metro que identifica que existem mais m�todos de msm nome
                                // Virtual pode ser usado pelas classes
    destructor Destroy; override;
    function ToString: string; override;     //CTRL J
    function Acelerar: string; override;

    // Acessado por qualquer classe, acesso p�blico

  published
    //  Acessado por qualquer classe, acesso p�blico, mas apenas em determinado componente
  end;

implementation

uses
  System.SysUtils;

{ TVeiculo }

constructor TVeiculo.Create;
begin
  FQuantidadeRodas := -1;
end;


destructor TVeiculo.Destroy;
begin

  inherited;
end;

procedure TVeiculo.SetMarca(const Value: String);
begin
  if Trim(Value) = '' then
    raise Exception.Create('Campo marca obrigat�rio!!!!!!!')    
    // RAISE levanta excess�o de um erro, ex: divis�o por zero
  else
    FMarca := Value;
end;

function TVeiculo.ToString: string;
var
  ClassNameResult: string;
begin
  ClassNameResult:= ClassName;
  Delete(ClassNameResult, 1,1);
  Result:= ClassNameResult;
end;

function TVeiculo.Acelerar: string;
var

begin
end;

end.
