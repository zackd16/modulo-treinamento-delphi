unit uUsuarios;

interface
type
  TUsuarios = class
  private
    FId: Integer;
    FNome: String;
    FEmail: string;
    FSenha: String;
    FTelefone: string;
    FCidade: string;
    FUF: string;

    procedure SetNome(const Value: String);
    procedure SetEmail(const Value: String);
    procedure SetSenha(const Value: String);
    procedure SetTelefone(const Value: String);
    procedure SetCidade(const Value: String);
    procedure SetUF(const Value: String);
  public
    property Id: Integer read FId write FId;
    property Nome: String read FNome write SetNome;
    property Email: string read FEmail write SetEmail;
    property Senha: String read FSenha write SetSenha;
    property Telefone: string read FTelefone write SetTelefone;
    property Cidade: string read FCidade write SetCidade;
    property UF: string read FUF write SetUF;
  end;
implementation

{ TUsuarios }

{ TUsuarios }
uses
  System.SysUtils;

procedure TUsuarios.SetCidade(const Value: String);
begin
  if Trim(Value) = '' then
    raise Exception.Create('Property TUsuarios.Cidade is required')
  else
  FCidade := Value;
end;

procedure TUsuarios.SetEmail(const Value: String);
begin
  if Trim(Value) = '' then
    raise Exception.Create('Property TUsuarios.Email is required')
  else
  FEmail := Value;
end;

procedure TUsuarios.SetNome(const Value: String);
begin
  if Trim(Value) = '' then
    raise Exception.Create('Property TUsuarios.Nome is required')
  else
  FNome := Value;
end;

procedure TUsuarios.SetSenha(const Value: String);
begin
  if Trim(Value) = '' then
    raise Exception.Create('Property TUsuarios.Senha is required')
  else
  FSenha := Value;
end;

procedure TUsuarios.SetTelefone(const Value: String);
begin
  if Trim(Value) = '' then
    raise Exception.Create('Property TUsuarios.Telefone is required')
  else
  FTelefone := Value;
end;

procedure TUsuarios.SetUF(const Value: String);
begin
  if Trim(Value) = '' then
    raise Exception.Create('Property TUsuarios.UF is required')
  else
  FUF := Value;
end;
end.
