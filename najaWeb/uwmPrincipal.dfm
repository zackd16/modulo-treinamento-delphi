object WebModule1: TWebModule1
  OldCreateOrder = False
  Actions = <
    item
      Default = True
      Name = 'DefaultHandler'
      PathInfo = '/'
      OnAction = WebModule1DefaultHandlerAction
    end
    item
      Name = 'acSobreAction'
      PathInfo = '/sobre'
      OnAction = WebModule1acSobreActionAction
    end
    item
      Name = 'acCategoriasSave'
      PathInfo = '/categorias/save'
      OnAction = WebModule1acCategoriasSaveAction
    end
    item
      Name = 'acCategoriasDelete'
      PathInfo = '/categorias/delete'
      OnAction = WebModule1acCategoriasDeleteAction
    end
    item
      Name = 'acCategoriasListagem'
      PathInfo = '/categorias'
      OnAction = WebModule1acCategoriasListagemAction
    end
    item
      Name = 'acUsuariosListagem'
      PathInfo = '/usuarios'
      OnAction = WebModule1acUsuariosListagemAction
    end
    item
      Name = 'acUsuariosSave'
      PathInfo = '/usuarios/save'
      OnAction = WebModule1acUsuariosSaveAction
    end
    item
      Name = 'acUsuariosDelete'
      PathInfo = '/usuarios/delete'
      OnAction = WebModule1acUsuariosDeleteAction
    end>
  Height = 230
  Width = 415
  object DSServer1: TDSServer
    Left = 96
    Top = 11
  end
  object DSHTTPWebDispatcher1: TDSHTTPWebDispatcher
    Server = DSServer1
    Filters = <>
    WebDispatch.PathInfo = 'datasnap*'
    Left = 96
    Top = 75
  end
  object DSServerClass1: TDSServerClass
    OnGetClass = DSServerClass1GetClass
    Server = DSServer1
    Left = 200
    Top = 11
  end
end
