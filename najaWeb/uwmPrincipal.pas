unit uwmPrincipal;

interface

uses
  System.SysUtils, System.Classes, Web.HTTPApp, Datasnap.DSHTTPCommon,
  Datasnap.DSHTTPWebBroker, Datasnap.DSServer,
  Datasnap.DSAuth, IPPeerServer, Datasnap.DSCommonServer, Datasnap.DSHTTP,
  System.JSON, Data.DBXCommon;

type
  TWebModule1 = class(TWebModule)
    DSHTTPWebDispatcher1: TDSHTTPWebDispatcher;
    DSServer1: TDSServer;
    DSServerClass1: TDSServerClass;
    procedure DSServerClass1GetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure WebModule1DefaultHandlerAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModule1acSobreActionAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModule1acCategoriasSaveAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModule1acCategoriasDeleteAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModule1acCategoriasListagemAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModule1acUsuariosListagemAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModule1acUsuariosSaveAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModule1acUsuariosDeleteAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WebModuleClass: TComponentClass = TWebModule1;

implementation


{$R *.dfm}

uses
  usvManager, Web.WebReq, uCategoriasController, uUsuariosController;

procedure TWebModule1.WebModule1acCategoriasDeleteAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
  CategoriasController: TCategoriasController;
begin
  CategoriasController:= TCategoriasController.Create;
  try
    if CategoriasController.Delete (Request.contentFields) then
       Response.Content := 'Success'
    else
       Response.Content := 'Error'
  finally
    CategoriasController.Free;
  end;
end;

procedure TWebModule1.WebModule1acCategoriasListagemAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
  CategoriaController: TCategoriasController;
begin
  CategoriaController:= TCategoriasController.Create;
  try
    Response.Content := CategoriaController.Listagem;
  finally
  CategoriaController.Free;
  end;
end;

procedure TWebModule1.WebModule1acCategoriasSaveAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
  CategoriasController: TCategoriasController;
begin
  CategoriasController:= TCategoriasController.Create;
  try
    if CategoriasController.Save (Request.contentFields) then
       Response.Content := 'Success'
    else
       Response.Content := 'Error'
  finally
    CategoriasController.Free;
  end;
end;

procedure TWebModule1.WebModule1acSobreActionAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  if request.contentfields.count = 1 then
    Response.Content := 'Seu nome �:' + request.contentfields.ValueFromIndex[0]
  else
    Response.Content := 'Naja Solu��es'; //?nome=raiz
end;

procedure TWebModule1.WebModule1acUsuariosDeleteAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
  UsuariosController: TUsuariosController;
begin
  UsuariosController:= TUsuariosController.Create;
  try
    if UsuariosController.Delete (Request.contentFields) then
       Response.Content := 'Success'
    else
       Response.Content := 'Error'
  finally
    UsuariosController.Free;
  end;
end;

procedure TWebModule1.WebModule1acUsuariosListagemAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
  UsuariosController: TUsuariosController;
begin
  UsuariosController:= TUsuariosController.Create;
  try
    Response.Content := UsuariosController.Listagem;
  finally
  UsuariosController.Free;
  end;
end;

procedure TWebModule1.WebModule1acUsuariosSaveAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
  UsuariosController: TUsuariosController;
begin
  UsuariosController:= TUsuariosController.Create;
  try
    if UsuariosController.Save (Request.contentFields) then
       Response.Content := 'Success'
    else
       Response.Content := 'Error'
  finally
    UsuariosController.Free;
  end;
end;

procedure TWebModule1.WebModule1DefaultHandlerAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  Response.Content :=
    '<html>'+
'<head>'+
'	<title> TESTE </title>'+
'</head>'+
'<body>'+
'	<button id="botao1" onclick="funcao()">Clique aqui</button>'+
' 	<script>'+
'		function funcao() {'+
'    	document.getElementById("quadrado").style.border = "border-radius: 100px";'+
'    	}'+
'	</script>'+
'</body>'+
'</html>';
end;

procedure TWebModule1.DSServerClass1GetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := usvManager.TServerManager;
end;

initialization
finalization
  Web.WebReq.FreeWebModules;

end.

