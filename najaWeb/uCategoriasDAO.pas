unit uCategoriasDAO;

interface

uses
  System.Classes,  udmDados, uCategoria,
  FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Stan.StorageJSON;

type
  TCategoriaDAO = class

  private
    FdmDados: TDMDados;
    FqrDados: TFDQuery;

  public
  constructor Create;
  destructor Destroy; override;

  function Save(aCategoria: TCategoria ): boolean;
  function Delete(aCategoria: TCategoria ): boolean;
  function Listagem: string;
  end;

implementation

{ TCategoriaDAO }

uses
  uCategoriasController;

constructor TCategoriaDAO.Create;
begin
  FDMDados := TDMDados.Create(nil);
  FQRDados := TFDQuery.Create(nil);
  FQRDados.Connection := FDMDados.fdConn;
end;

function TCategoriaDAO.Delete(aCategoria: TCategoria): boolean;
begin
  FqrDados.SQL.Text :=
  'DELETE FROM CATEGORIAS '+
  'WHERE ID = :ID';

  FqrDados.ParamByName ('ID').AsInteger := aCategoria.Id;
  FqrDados.ExecSQL;
  Result := true;
end;

destructor TCategoriaDAO.Destroy;
begin
  FqrDados.Free;
  FDMDados.Free;
  inherited;
end;

function TCategoriaDAO.Listagem: string;
var
  StrStream: TStringStream;
begin
  StrStream:= TStringStream.Create;
  try
    FqrDados.SQL.Text :=
      'SELECT * FROM CATEGORIAS ';

    FqrDados.Open;
    FqrDados.SaveToStream(StrStream, sfJSON);
    Result := StrStream.DataString;
  finally
    StrStream.Free;
  end;
end;

function TCategoriaDAO.Save(aCategoria: TCategoria ): boolean;
begin
  if aCategoria.ID = 0 then
  begin
    FqrDados.SQL.Text :=
   'INSERT INTO CATEGORIAS(NOME)'+
   'VALUES (:NOME)';

   FqrDados.ParamByName('NOME').AsString := aCategoria.Nome;
  end
  else
  begin
    FqrDados.SQL.Text :=
   'UPDATE CATEGORIAS SET NOME = :NOME'+
   'WHERE ID = :ID';

   FqrDados.ParamByName('ID').AsInteger := aCategoria.Id;
   FqrDados.ParamByName('NOME').AsString := aCategoria.Nome;
  end;
   fqrDados.ExecSQL;
   Result := true;
end;

end.
