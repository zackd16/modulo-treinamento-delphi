unit uUsuariosDAO;

interface

uses
  System.Classes,  udmDados, uUsuarios,
  FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Stan.StorageJSON;

type
  TUsuariosDAO = class

  private
    FdmDados: TDMDados;
    FqrDados: TFDQuery;

  public
  constructor Create;
  destructor Destroy; override;

  function Save(aUsuarios: TUsuarios ): boolean;
  function Delete(aUsuarios: TUsuarios ): boolean;
  function Listagem: string;
  end;

implementation

uses
  uUsuariosController;


{ TUsuariosDAO }

constructor TUsuariosDAO.Create;
begin
  FDMDados := TDMDados.Create(nil);
  FQRDados := TFDQuery.Create(nil);
  FQRDados.Connection := FDMDados.fdConn;
end;

function TUsuariosDAO.Delete(aUsuarios: TUsuarios): boolean;
begin
  FqrDados.SQL.Text :=
  'DELETE FROM USUARIOS '+
  'WHERE ID = :ID';

  FqrDados.ParamByName ('ID').AsInteger := aUsuarios.Id;
  FqrDados.ExecSQL;
  Result := true;
end;

destructor TUsuariosDAO.Destroy;
begin
  FqrDados.Free;
  FDMDados.Free;
  inherited;
end;

function TUsuariosDAO.Listagem: string;
var
  StrStream: TStringStream;
begin
  StrStream:= TStringStream.Create;
  try
    FqrDados.SQL.Text :=
      'SELECT * FROM USUARIO ';

    FqrDados.Open;
    FqrDados.SaveToStream(StrStream, sfJSON);
    Result := StrStream.DataString;
  finally
    StrStream.Free;
  end;
end;

function TUsuariosDAO.Save(aUsuarios: TUsuarios): boolean;
begin
  if aUsuarios.ID = 0 then
  begin
    FqrDados.SQL.Text :=
   'INSERT INTO USUARIO(NOME, EMAIL, SENHA, TELEFONE, CIDADE, UF)'+
   'VALUES (:NOME,:EMAIL,:SENHA,:TELEFONE,:CIDADE,:UF)';

   FqrDados.ParamByName('NOME').AsString := aUsuarios.Nome;
   FqrDados.ParamByName('EMAIL').AsString := aUsuarios.Email;
   FqrDados.ParamByName('SENHA').AsString := aUsuarios.Senha;
   FqrDados.ParamByName('TELEFONE').AsString := aUsuarios.Telefone;
   FqrDados.ParamByName('CIDADE').AsString := aUsuarios.Cidade;
   FqrDados.ParamByName('UF').AsString := aUsuarios.UF;
  end
  else
  begin
    FqrDados.SQL.Text :=
   'UPDATE CATEGORIAS SET NOME = :NOME'+
   'WHERE ID = :ID';

   FqrDados.ParamByName('NOME').AsString := aUsuarios.Nome;
   FqrDados.ParamByName('EMAIL').AsString := aUsuarios.Email;
   FqrDados.ParamByName('SENHA').AsString := aUsuarios.Senha;
   FqrDados.ParamByName('TELEFONE').AsString := aUsuarios.Telefone;
   FqrDados.ParamByName('CIDADE').AsString := aUsuarios.Cidade;
   FqrDados.ParamByName('UF').AsString := aUsuarios.UF;
  end;
   fqrDados.ExecSQL;
   Result := true;
end;

end.
