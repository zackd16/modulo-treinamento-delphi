unit uCategoriasController;

interface

uses
  System.Classes, uCategoriasDAO, System.SysUtils;

type
  TCategoriasController = class
  private
    FCategoriaDAO: TCategoriaDAO;
  public
  constructor Create;
  destructor Destroy; override;


  function Save(Parametros: TStrings ): boolean;
  function Delete(Parametros: TStrings ): boolean;
  function Listagem: string;
  end;

implementation

{ TCategoriasController }
uses
  uCategoria;

constructor TCategoriasController.Create;
begin
  FCategoriaDAO := TCategoriaDAO.Create;
end;

function TCategoriasController.Delete(Parametros: TStrings): boolean;
var
  Categoria: TCategoria;
begin
  Categoria := TCategoria.Create;
  try
    Categoria.Id := StrToInt(Parametros.Values['id']);
    result := FCategoriaDAO.delete(Categoria);
  finally
  Categoria.Free;
 end;
end;

destructor TCategoriasController.Destroy;
begin
  FCategoriaDAO.Free;
  inherited;
end;

function TCategoriasController.Listagem: string;
begin
  Result := FCAtegoriaDAO.Listagem;
end;

function TCategoriasController.Save(Parametros: TStrings): boolean;
var
  Categoria : TCategoria;
begin
Categoria := TCategoria.Create;
  try
    if Parametros.Values['id']='' then
      Categoria.Id:= 0
    else
      Categoria.ID := StrToInt(Parametros.Values['Id']);

  Categoria.Nome := Parametros.Values['nome'];
  result := FCategoriaDAO.Save(Categoria);
  finally
   Categoria.Free;
 end;

end;

end.
