unit uCategoria;

interface

type
  TCategoria = class
  private
    FId: Integer;
    FNome: String;
    procedure SetNome(const Value: String);
  public
    property Id: Integer read FId write FId;
    property Nome: String read FNome write SetNome;

  end;

implementation

uses
   System.SysUtils;
{ TCategoria }

procedure TCategoria.SetNome(const Value: String);
begin
  if Trim(Value) = '' then
    raise Exception.Create('Property TCategoria.Nome is required')
  else
  FNome := Value;
end;

end.
