unit uUsuariosController;

interface

uses
  System.Classes, uUsuariosDAO, System.SysUtils;

type
  TUsuariosController = class
  private
    FUsuariosDAO: TUsuariosDAO;
  public
  constructor Create;
  destructor Destroy; override;

  function Save(Parametros: TStrings ): boolean;
  function Delete(Parametros: TStrings ): boolean;
  function Listagem: string;
  end;

implementation

uses
  uUsuarios;


{ TUsuariosController }

constructor TUsuariosController.Create;
begin
  FUsuariosDAO := TUsuariosDAO.Create;
end;

function TUsuariosController.Delete(Parametros: TStrings): boolean;
var
  Usuarios: TUsuarios;
begin
  Usuarios := TUsuarios.Create;
  try
    Usuarios.Id := StrToInt(Parametros.Values['id']);
    result := FUsuariosDAO.delete(Usuarios);
  finally
  Usuarios.Free;
 end;
end;

destructor TUsuariosController.Destroy;
begin
  FUsuariosDAO.Free;
  inherited;
end;

function TUsuariosController.Listagem: string;
begin
  Result := FUsuariosDAO.Listagem;
end;

function TUsuariosController.Save(Parametros: TStrings): boolean;
var
  Usuarios : TUsuarios;
begin
Usuarios := TUsuarios.Create;
  try
    if Parametros.Values['id']='' then
      Usuarios.Id:= 0
    else
      Usuarios.ID := StrToInt(Parametros.Values['Id']);

  Usuarios.Nome := Parametros.Values['NOME'];
  Usuarios.Nome := Parametros.Values['EMAIL'];
  Usuarios.Nome := Parametros.Values['SENHA'];
  Usuarios.Nome := Parametros.Values['TELEFONE'];
  Usuarios.Nome := Parametros.Values['CIDADE'];
  Usuarios.Nome := Parametros.Values['UF'];
  result := FUsuariosDAO.Save(Usuarios);
  finally
   Usuarios.Free;
 end;

end;

end.
