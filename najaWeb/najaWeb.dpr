program najaWeb;
{$APPTYPE GUI}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  uPrincipal in 'uPrincipal.pas' {fmPrincipal},
  usvManager in 'usvManager.pas',
  uwmPrincipal in 'uwmPrincipal.pas' {WebModule1: TWebModule},
  udmDados in 'udmDados.pas' {dmDados: TDataModule},
  uCategoriasController in 'uCategoriasController.pas',
  uCategoriasDAO in 'uCategoriasDAO.pas',
  uCategoria in 'uCategoria.pas',
  uUsuarios in 'uUsuarios.pas',
  uUsuariosDAO in 'uUsuariosDAO.pas',
  uUsuariosController in 'uUsuariosController.pas';

{$R *.res}

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  Application.Initialize;
  Application.CreateForm(TfmPrincipal, fmPrincipal);
  Application.Run;
end.
