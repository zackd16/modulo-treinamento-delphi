unit uListagemCategorias;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, RLReport, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfmListagemCategorias = class(TForm)
    rpPrincipal: TRLReport;
    RLBand1: TRLBand;
    RLLabel1: TRLLabel;
    RLBand2: TRLBand;
    RLLabel2: TRLLabel;
    RLLabel3: TRLLabel;
    qrDados: TFDQuery;
    dsDados: TDataSource;
    RLBand3: TRLBand;
    qrDadosID: TFDAutoIncField;
    qrDadosNOME: TStringField;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    RLBand4: TRLBand;
    RLSystemInfo1: TRLSystemInfo;
    RLBand5: TRLBand;
    RLSystemInfo2: TRLSystemInfo;
    RLLabel4: TRLLabel;
    procedure rpPrincipalBeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmListagemCategorias: TfmListagemCategorias;

implementation
uses
    udmDados;
{$R *.dfm}

procedure TfmListagemCategorias.rpPrincipalBeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  qrDados.Open;
end;

end.
