object fmListagemProdutosPorCategoria: TfmListagemProdutosPorCategoria
  Left = 0
  Top = 0
  Caption = 'fmListagemProdutosPorCategoria'
  ClientHeight = 477
  ClientWidth = 786
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object rpPrincipal: TRLReport
    Left = 0
    Top = 0
    Width = 794
    Height = 1123
    DataSource = dsDados
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    BeforePrint = rpPrincipalBeforePrint
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 51
      BandType = btHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = False
      Borders.DrawRight = False
      Borders.DrawBottom = True
    end
    object RLLabel1: TRLLabel
      Left = 241
      Top = 53
      Width = 313
      Height = 22
      Caption = 'Listagem Produtos por Categoria'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object RLGroup1: TRLGroup
      Left = 38
      Top = 89
      Width = 718
      Height = 104
      DataFields = 'NOME'
      object RLBand2: TRLBand
        Left = 0
        Top = 0
        Width = 718
        Height = 49
        BandType = btColumnHeader
        object RLLabel3: TRLLabel
          Left = 16
          Top = 6
          Width = 18
          Height = 16
          Caption = 'Id:'
        end
        object RLDBText2: TRLDBText
          Left = 40
          Top = 6
          Width = 16
          Height = 16
          DataField = 'ID'
          DataSource = dsDados
          Text = ''
        end
        object RLLabel2: TRLLabel
          Left = 149
          Top = 6
          Width = 53
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object RLDBText1: TRLDBText
          Left = 216
          Top = 6
          Width = 32
          Height = 13
          DataField = 'NOME'
          DataSource = dsDados
          Text = ''
        end
        object RLLabel4: TRLLabel
          Left = 64
          Top = 33
          Width = 53
          Height = 16
          Caption = 'Produto:'
        end
        object RLLabel5: TRLLabel
          Left = 456
          Top = 30
          Width = 42
          Height = 16
          Caption = 'Pre'#231'o:'
        end
      end
      object RLBand3: TRLBand
        Left = 0
        Top = 49
        Width = 718
        Height = 32
        object RLDBText3: TRLDBText
          Left = 64
          Top = 6
          Width = 47
          Height = 16
          DataField = 'TITULO'
          DataSource = dsDados
          Text = ''
        end
        object RLDBText5: TRLDBText
          Left = 456
          Top = 3
          Width = 50
          Height = 16
          DataField = 'PRECO'
          DataSource = dsDados
          Text = ''
        end
      end
    end
    object RLBand4: TRLBand
      Left = 38
      Top = 193
      Width = 718
      Height = 32
      BandType = btFooter
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = False
      object RLSystemInfo1: TRLSystemInfo
        Left = 616
        Top = 6
        Width = 87
        Height = 16
        Alignment = taRightJustify
        Info = itPageNumber
        Text = ''
      end
    end
  end
  object qrDados: TFDQuery
    Active = True
    Connection = dmDados.fdConn
    SQL.Strings = (
      'SELECT '
      #9'CATE.ID,'
      #9'CATE.NOME,'
      #9'PROD.TITULO,'
      #9'PROD.PRECO'
      'FROM CATEGORIAS AS CATE'
      'INNER JOIN PRODUTOS AS PROD ON PROD.CATEGORIA_ID = CATE.ID'
      'ORDER BY CATE.NOME')
    Left = 640
    Top = 256
  end
  object dsDados: TDataSource
    DataSet = qrDados
    Left = 632
    Top = 328
  end
end
