program olz;

uses
  Vcl.Forms,
  uPrincipal in 'uPrincipal.pas' {fmPrincipal},
  uCadastroBase in 'uCadastroBase.pas' {fmCadastroBase},
  uCadastroCategorias in 'uCadastroCategorias.pas' {fmCadastroCategorias},
  udmDados in 'udmDados.pas' {dmDados: TDataModule},
  uCadastroUsuarios in 'uCadastroUsuarios.pas' {fmCadastroUsuarios},
  uCadastroProdutos in 'uCadastroProdutos.pas' {fmCadastroProdutos},
  uPesquisaProdutos in 'uPesquisaProdutos.pas' {fmPesquisaProdutos},
  uListagemCategorias in 'uListagemCategorias.pas' {fmListagemCategorias},
  uListagemUF in 'uListagemUF.pas' {fmListagemUF},
  uListagemProdutosPorCategoria in 'uListagemProdutosPorCategoria.pas' {fmListagemProdutosPorCategoria};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmPrincipal, fmPrincipal);
  Application.CreateForm(TdmDados, dmDados);
  Application.Run;
end.
