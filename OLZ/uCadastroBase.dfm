object fmCadastroBase: TfmCadastroBase
  Left = 0
  Top = 0
  Caption = 'Cadastro Base'
  ClientHeight = 441
  ClientWidth = 671
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pcPrincipal: TPageControl
    Left = 0
    Top = 0
    Width = 671
    Height = 441
    ActivePage = tsEdits
    Align = alClient
    TabOrder = 0
    object tsDados: TTabSheet
      Caption = 'tsDados'
      object pnButtonsDados: TPanel
        Left = 531
        Top = 0
        Width = 132
        Height = 413
        Align = alRight
        BevelOuter = bvNone
        Color = 14113695
        ParentBackground = False
        TabOrder = 0
        object btInserir: TButton
          Left = 16
          Top = 16
          Width = 100
          Height = 41
          Caption = 'Inserir'
          TabOrder = 0
          OnClick = btInserirClick
        end
        object btEditar: TButton
          Left = 16
          Top = 72
          Width = 100
          Height = 41
          Caption = 'Editar'
          TabOrder = 1
          OnClick = btEditarClick
        end
        object btExcluir: TButton
          Left = 16
          Top = 128
          Width = 100
          Height = 41
          Caption = 'Excluir'
          TabOrder = 2
          OnClick = btExcluirClick
        end
        object nvDados: TDBNavigator
          Left = 0
          Top = 384
          Width = 132
          Height = 29
          DataSource = dsDados
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
          Align = alBottom
          TabOrder = 3
        end
      end
      object grDados: TDBGrid
        Left = 0
        Top = 0
        Width = 531
        Height = 413
        Align = alClient
        Color = clWhite
        DataSource = dsDados
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgTitleClick, dgTitleHotTrack]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object tsEdits: TTabSheet
      Caption = 'tsEdits'
      ImageIndex = 1
      object pnButtonsEdits: TPanel
        Left = 531
        Top = 0
        Width = 132
        Height = 413
        Align = alRight
        BevelOuter = bvNone
        Color = 14113695
        ParentBackground = False
        TabOrder = 0
        object btSalvar: TButton
          Left = 16
          Top = 16
          Width = 100
          Height = 41
          Caption = 'Salvar'
          TabOrder = 0
          OnClick = btSalvarClick
        end
        object btCancelar: TButton
          Left = 16
          Top = 72
          Width = 100
          Height = 41
          Caption = 'Cancelar'
          TabOrder = 1
          OnClick = btCancelarClick
        end
      end
    end
  end
  object qrDados: TFDQuery
    Connection = dmDados.fdConn
    Left = 592
    Top = 224
  end
  object dsDados: TDataSource
    DataSet = qrDados
    Left = 592
    Top = 288
  end
end
