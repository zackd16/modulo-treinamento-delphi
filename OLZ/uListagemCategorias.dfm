object fmListagemCategorias: TfmListagemCategorias
  Left = 0
  Top = 0
  Caption = 'fmListagemCategorias'
  ClientHeight = 488
  ClientWidth = 795
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object rpPrincipal: TRLReport
    Left = 0
    Top = 0
    Width = 794
    Height = 1123
    Borders.Sides = sdCustom
    Borders.DrawLeft = False
    Borders.DrawTop = False
    Borders.DrawRight = False
    Borders.DrawBottom = False
    DataSource = dsDados
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    BeforePrint = rpPrincipalBeforePrint
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 43
      BandType = btHeader
    end
    object RLLabel1: TRLLabel
      Left = 304
      Top = 49
      Width = 224
      Height = 22
      Caption = 'Listagem de Categorias'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object RLBand2: TRLBand
      Left = 38
      Top = 81
      Width = 718
      Height = 24
      BandType = btColumnHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = True
      object RLLabel2: TRLLabel
        Left = 16
        Top = 6
        Width = 16
        Height = 16
        Caption = 'ID'
      end
      object RLLabel3: TRLLabel
        Left = 72
        Top = 6
        Width = 62
        Height = 16
        Caption = 'Descri'#231#227'o'
      end
    end
    object RLBand3: TRLBand
      Left = 38
      Top = 105
      Width = 718
      Height = 36
      object RLDBText1: TRLDBText
        Left = 16
        Top = 13
        Width = 32
        Height = 16
        DataField = 'ID'
        DataSource = dsDados
        Text = ''
      end
      object RLDBText2: TRLDBText
        Left = 72
        Top = 13
        Width = 71
        Height = 16
        DataField = 'NOME'
        DataSource = dsDados
        Text = ''
      end
    end
    object RLBand4: TRLBand
      Left = 38
      Top = 173
      Width = 718
      Height = 24
      BandType = btFooter
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = False
      object RLSystemInfo1: TRLSystemInfo
        Left = 628
        Top = 6
        Width = 87
        Height = 16
        Alignment = taRightJustify
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = False
        Borders.DrawRight = False
        Borders.DrawBottom = False
        Info = itPageNumber
        Text = ''
      end
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 141
      Width = 718
      Height = 32
      BandType = btSummary
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = False
      object RLSystemInfo2: TRLSystemInfo
        Left = 131
        Top = 10
        Width = 79
        Height = 16
        Alignment = taRightJustify
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = False
        Borders.DrawRight = False
        Borders.DrawBottom = False
        Info = itDetailCount
        Text = ''
      end
      object RLLabel4: TRLLabel
        Left = 16
        Top = 6
        Width = 109
        Height = 16
        Caption = 'Total de Registros'
      end
    end
  end
  object qrDados: TFDQuery
    Connection = dmDados.fdConn
    SQL.Strings = (
      'select * from Categorias')
    Left = 648
    Top = 240
    object qrDadosID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object qrDadosNOME: TStringField
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 50
    end
  end
  object dsDados: TDataSource
    DataSet = qrDados
    Left = 648
    Top = 304
  end
end
