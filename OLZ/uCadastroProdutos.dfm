inherited fmCadastroProdutos: TfmCadastroProdutos
  Caption = 'fmCadastroProdutos'
  ClientHeight = 503
  ExplicitHeight = 542
  PixelsPerInch = 96
  TextHeight = 13
  inherited pcPrincipal: TPageControl
    Height = 503
    ExplicitHeight = 503
    inherited tsDados: TTabSheet
      ExplicitHeight = 475
      inherited pnButtonsDados: TPanel
        Height = 475
        ExplicitHeight = 475
        inherited nvDados: TDBNavigator
          Top = 446
          Hints.Strings = ()
          ExplicitTop = 446
        end
      end
      inherited grDados: TDBGrid
        Height = 475
      end
    end
    inherited tsEdits: TTabSheet
      ExplicitLeft = 8
      ExplicitTop = 16
      ExplicitHeight = 475
      object Label1: TLabel [0]
        Left = 16
        Top = 8
        Width = 11
        Height = 13
        Caption = 'ID'
        FocusControl = edId
      end
      object Label2: TLabel [1]
        Left = 16
        Top = 48
        Width = 36
        Height = 13
        Caption = 'TITULO'
        FocusControl = edTitulo
      end
      object Label3: TLabel [2]
        Left = 16
        Top = 88
        Width = 59
        Height = 13
        Caption = 'DESCRICAO'
      end
      object Label4: TLabel [3]
        Left = 16
        Top = 189
        Width = 34
        Height = 13
        Caption = 'PRECO'
      end
      object Label7: TLabel [4]
        Left = 16
        Top = 232
        Width = 13
        Height = 13
        Caption = 'UF'
      end
      object Label10: TLabel [5]
        Left = 347
        Top = 208
        Width = 40
        Height = 13
        Caption = 'IMAGEM'
        FocusControl = imImagem
      end
      object Label5: TLabel [6]
        Left = 16
        Top = 278
        Width = 59
        Height = 13
        Caption = 'CATEGORIA'
        FocusControl = edCategorias
      end
      inherited pnButtonsEdits: TPanel
        Height = 475
        ExplicitTop = 1
        ExplicitHeight = 475
      end
      object edId: TDBEdit
        Left = 16
        Top = 27
        Width = 134
        Height = 21
        DataField = 'ID'
        DataSource = dsDados
        TabOrder = 1
      end
      object edTitulo: TDBEdit
        Left = 16
        Top = 64
        Width = 441
        Height = 21
        DataField = 'TITULO'
        DataSource = dsDados
        TabOrder = 2
      end
      object imImagem: TDBImage
        Left = 280
        Top = 227
        Width = 177
        Height = 181
        DataField = 'IMAGEM'
        DataSource = dsDados
        Stretch = True
        TabOrder = 8
      end
      object edPreco: TDBEdit
        Left = 16
        Top = 205
        Width = 134
        Height = 21
        DataField = 'PRECO'
        DataSource = dsDados
        TabOrder = 4
      end
      object lkUf: TDBLookupComboBox
        Left = 16
        Top = 251
        Width = 49
        Height = 21
        DataField = 'UF_ID'
        DataSource = dsDados
        KeyField = 'id'
        ListField = 'id'
        ListSource = dsUf
        TabOrder = 5
      end
      object btCarregar: TButton
        Left = 382
        Top = 427
        Width = 75
        Height = 25
        Caption = 'Carregar'
        TabOrder = 10
        OnClick = btCarregarClick
      end
      object btLimpar: TButton
        Left = 280
        Top = 427
        Width = 75
        Height = 25
        Caption = 'Apagar'
        TabOrder = 9
        OnClick = btLimparClick
      end
      object edCategorias: TDBEdit
        Left = 16
        Top = 297
        Width = 25
        Height = 21
        DataField = 'CATEGORIA_ID'
        DataSource = dsDados
        TabOrder = 6
      end
      object lkCategorias: TDBLookupComboBox
        Left = 47
        Top = 297
        Width = 145
        Height = 21
        DataField = 'CATEGORIA_ID'
        DataSource = dsDados
        KeyField = 'id'
        ListField = 'nome'
        ListSource = dsCategorias
        TabOrder = 7
      end
      object mmDescricao: TDBMemo
        Left = 16
        Top = 107
        Width = 441
        Height = 79
        DataField = 'DESCRICAO'
        DataSource = dsDados
        TabOrder = 3
      end
    end
  end
  inherited qrDados: TFDQuery
    BeforePost = qrDadosBeforePost
    SQL.Strings = (
      
        'select ID, TITULO, DESCRICAO, PRECO, UF_ID, CATEGORIA_ID, USUARI' +
        'O_ID, IMAGEM '
      'from produtos')
    Left = 552
    Top = 176
    object qrDadosID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object qrDadosTITULO: TStringField
      FieldName = 'TITULO'
      Origin = 'TITULO'
      Required = True
      Size = 100
    end
    object qrDadosDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 255
    end
    object qrDadosPRECO: TCurrencyField
      FieldName = 'PRECO'
      Origin = 'PRECO'
      Required = True
    end
    object qrDadosUF_ID: TStringField
      FieldName = 'UF_ID'
      Origin = 'UF_ID'
      Required = True
      FixedChar = True
      Size = 2
    end
    object qrDadosCATEGORIA_ID: TIntegerField
      FieldName = 'CATEGORIA_ID'
      Origin = 'CATEGORIA_ID'
      Required = True
    end
    object qrDadosUSUARIO_ID: TIntegerField
      FieldName = 'USUARIO_ID'
      Origin = 'USUARIO_ID'
      Required = True
    end
    object qrDadosIMAGEM: TBlobField
      FieldName = 'IMAGEM'
      Origin = 'IMAGEM'
      Required = True
    end
  end
  inherited dsDados: TDataSource
    Left = 616
    Top = 176
  end
  object qrUf: TFDQuery
    Connection = dmDados.fdConn
    SQL.Strings = (
      'select id from uf')
    Left = 551
    Top = 320
  end
  object dsUf: TDataSource
    DataSet = qrUf
    Left = 620
    Top = 320
  end
  object odImagem: TOpenDialog
    Filter = 'Imagem JPEG|*.jpg'
    Left = 476
    Top = 336
  end
  object qrCategorias: TFDQuery
    Connection = dmDados.fdConn
    SQL.Strings = (
      'select id,nome from Categorias')
    Left = 551
    Top = 248
    object qrCategoriasid: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object qrCategoriasnome: TStringField
      FieldName = 'nome'
      Origin = 'nome'
      Required = True
      Size = 50
    end
  end
  object dsCategorias: TDataSource
    DataSet = qrCategorias
    Left = 615
    Top = 248
  end
end
