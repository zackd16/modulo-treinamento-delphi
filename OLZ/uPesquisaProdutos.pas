unit uPesquisaProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.DBCtrls, Vcl.ExtCtrls;

type
  TfmPesquisaProdutos = class(TForm)
    pnPesquisa: TPanel;
    Label1: TLabel;
    edBusca: TEdit;
    cbFiltro: TComboBox;
    Label2: TLabel;
    pnInfo: TPanel;
    imImagem: TDBImage;
    mmDescricao: TDBMemo;
    grDados: TDBGrid;
    qrDados: TFDQuery;
    dsDados: TDataSource;
    qrDadosID: TFDAutoIncField;
    qrDadosTITULO: TStringField;
    qrDadosDESCRICAO: TStringField;
    qrDadosIMAGEM: TBlobField;
    qrDadosCategoria: TStringField;
    qrDadosUF: TStringField;
    qrDadosPRECO: TCurrencyField;
    Label3: TLabel;
    cbOrdem: TComboBox;
    Label4: TLabel;
    procedure FormShow(Sender: TObject);
    procedure edBuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbFiltroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure Buscar;
  public
    { Public declarations }
  end;

var
  fmPesquisaProdutos: TfmPesquisaProdutos;

implementation
uses
  udmDados;
{$R *.dfm}

procedure TfmPesquisaProdutos.Buscar;
begin
  qrDados.Close;
  qrDados.SQL.Text:=
    'SELECT'+
    ' PROD.ID,'+
    ' PROD.TITULO,'+
    ' PROD.DESCRICAO,'+
    ' PROD.PRECO,'+
    ' PROD.IMAGEM,'+
    ' CATE.NOME AS CATEGORIA,'+
    ' UF.ID AS UF'+
    'FROM PRODUTOS AS PROD'+
    'INNER JOIN CATEGORIAS AS CATE ON CATE.ID = PROD.CATEGORIA_ID'+
    'INNER JOIN UF ON UF.ID = PROD.UF_ID';

  if trim(edBusca.text) <> '' then
  begin
    case cbFiltro.ItemIndex of
      0: qrDados.SQL.Text := qrDados.SQL.Text + 'WHERE PROD.TITULO LIKE''%'+ edBusca.Text+'%''';
      1: qrDados.SQL.Text := qrDados.SQL.Text + 'WHERE CATE.NOME LIKE''%'+ edBusca.Text+'%''';
      2: qrDados.SQL.Text := qrDados.SQL.Text + 'WHERE UF.ID LIKE''%'+ edBusca.Text+'%''';
    end;
  end;

  case cbOrdem.ItemIndex of
    1: qrDados.SQL.Text := qrDados.SQL.Text + 'ORDER BY PROD.TITULO DESC';
    2: qrDados.SQL.Text := qrDados.SQL.Text + 'ORDER BY PROD.PRECO ASC';
    3: qrDados.SQL.Text := qrDados.SQL.Text + 'ORDER BY PROD.PRECO DESC';
  else
    qrDados.SQL.Text := qrDados.SQL.Text + 'ORDER BY PROD.TITULO';
  end;

  qrDados.Open;
end;

procedure TfmPesquisaProdutos.cbFiltroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
    Buscar;
end;

procedure TfmPesquisaProdutos.edBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
    Buscar;
end;

procedure TfmPesquisaProdutos.FormShow(Sender: TObject);
begin
  qrDados.Open;
end;

end.
