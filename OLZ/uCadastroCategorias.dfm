inherited fmCadastroCategorias: TfmCadastroCategorias
  Caption = 'Cadastro de Categorias'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pcPrincipal: TPageControl
    inherited tsDados: TTabSheet
      inherited pnButtonsDados: TPanel
        inherited nvDados: TDBNavigator
          Hints.Strings = ()
        end
      end
    end
    inherited tsEdits: TTabSheet
      Font.Charset = ANSI_CHARSET
      Font.Height = -12
      Font.Name = 'Arial'
      ParentFont = False
      object Label1: TLabel [0]
        Left = 16
        Top = 16
        Width = 12
        Height = 16
        Caption = 'Id'
        FocusControl = edId
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel [1]
        Left = 16
        Top = 67
        Width = 37
        Height = 16
        Caption = 'Nome'
        FocusControl = edNome
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edId: TDBEdit
        Left = 16
        Top = 38
        Width = 101
        Height = 23
        DataField = 'ID'
        DataSource = dsDados
        TabOrder = 1
      end
      object edNome: TDBEdit
        Left = 16
        Top = 89
        Width = 345
        Height = 23
        DataField = 'NOME'
        DataSource = dsDados
        TabOrder = 2
      end
    end
  end
  inherited qrDados: TFDQuery
    SQL.Strings = (
      'select * from Categorias')
    Left = 552
    Top = 200
    object qrDadosID: TFDAutoIncField
      DisplayLabel = 'Id'
      DisplayWidth = 10
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object qrDadosNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 50
    end
  end
  inherited dsDados: TDataSource
    Left = 616
    Top = 200
  end
end
