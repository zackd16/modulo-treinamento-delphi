unit uCadastroUsuarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uCadastroBase, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.Grids, Vcl.DBGrids, Vcl.DBCtrls, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.Mask;

type
  TfmCadastroUsuarios = class(TfmCadastroBase)
    qrDadosID: TIntegerField;
    qrDadosNome: TStringField;
    qrDadosEmail: TStringField;
    qrDadosTelefone: TStringField;
    qrDadosCidade: TStringField;
    qrDadosUF: TStringField;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    edId: TDBEdit;
    Label9: TLabel;
    edNome: TDBEdit;
    Label10: TLabel;
    edEmail: TDBEdit;
    Label11: TLabel;
    edTelefone: TDBEdit;
    Label12: TLabel;
    edCidade: TDBEdit;
    Label14: TLabel;
    edSenha: TDBEdit;
    qrDadosSenha: TStringField;
    qrUf: TFDQuery;
    qrUfid: TStringField;
    Label13: TLabel;
    lkUF: TDBLookupComboBox;
    dsUf: TDataSource;
    procedure btInserirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btEditarClick(Sender: TObject);
  private
  protected
    function ValidarObrigatorios: boolean; override;
  public
    { Public declarations }
  end;

var
  fmCadastroUsuarios: TfmCadastroUsuarios;

implementation

uses
  System.StrUtils, udmDados;
  {$R *.dfm}

procedure TfmCadastroUsuarios.btEditarClick(Sender: TObject);
begin
  inherited;
  edNome.SetFocus;
end;

procedure TfmCadastroUsuarios.btInserirClick(Sender: TObject);
begin
  inherited;
  edNome.SetFocus;
end;

procedure TfmCadastroUsuarios.FormClose(Sender: TObject;
var Action: TCloseAction);
begin
  inherited;
  fmCadastroUsuarios := nil;
end;

procedure TfmCadastroUsuarios.FormShow(Sender: TObject);
begin
  inherited;
  qrUf.Open;
end;

function TfmCadastroUsuarios.ValidarObrigatorios: boolean;
begin
  if Trim(edNome.Text) = '' then
  begin
    ShowMessage('Informe o nome');
    edNome.SetFocus;
    Exit(false);
  end;
  
  if Trim(edEmail.Text) = '' then
  begin
    ShowMessage('Informe o email');
    edEmail.SetFocus;
    Exit(false);
  end;

  if not (AnsiContainsStr(edEmail.Text, '@')) or not (AnsiContainsText(edEmail.Text, '.com')) then
  begin
    ShowMessage('Email invalido');
    edEmail.SetFocus;
    Exit(false);
  end;
  

  if Trim(edSenha.Text) = '' then
  begin
    ShowMessage('Informe a senha');
    edSenha.SetFocus;
    Exit(false);
  end;
  
  if Trim(edTelefone.Text) = '' then
  begin
    ShowMessage('Informe o telefone');
    edTelefone.SetFocus;
    Exit(false);
  end;
  
  if Trim(edCidade.Text) = '' then
  begin
    ShowMessage('Informe a cidade');
    edCidade.SetFocus;
    Exit(false);
  end;
  
  if Trim(lkUF.Text) = '' then
  begin
    ShowMessage('Informe a unidade federativa');
    lkUF.SetFocus;
    Exit(false);
  end;
  
  Result := inherited ValidarObrigatorios;
end;

end.
