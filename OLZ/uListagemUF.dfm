object fmListagemUF: TfmListagemUF
  Left = 0
  Top = 0
  Caption = 'fmListagemUF'
  ClientHeight = 583
  ClientWidth = 766
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object rpPrincipal: TRLReport
    Left = 0
    Top = 0
    Width = 794
    Height = 1123
    DataSource = dsDados
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    BeforePrint = rpPrincipalBeforePrint
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 27
      BandType = btHeader
      object RLLabel1: TRLLabel
        Left = 304
        Top = 3
        Width = 148
        Height = 22
        Alignment = taCenter
        Caption = 'Listagem de UF'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object RLBand2: TRLBand
      Left = 38
      Top = 65
      Width = 718
      Height = 26
      BandType = btColumnHeader
      object RLLabel2: TRLLabel
        Left = 16
        Top = 6
        Width = 21
        Height = 16
        Caption = 'UF'
      end
      object RLLabel3: TRLLabel
        Left = 72
        Top = 6
        Width = 43
        Height = 16
        Caption = 'NOME'
      end
    end
    object RLBand3: TRLBand
      Left = 38
      Top = 91
      Width = 718
      Height = 32
      object RLDBText1: TRLDBText
        Left = 16
        Top = 13
        Width = 32
        Height = 16
        DataField = 'ID'
        DataSource = dsDados
        Text = ''
      end
      object RLDBText2: TRLDBText
        Left = 72
        Top = 13
        Width = 71
        Height = 16
        DataField = 'NOME'
        DataSource = dsDados
        Text = ''
      end
    end
    object RLBand4: TRLBand
      Left = 38
      Top = 153
      Width = 718
      Height = 30
      BandType = btFooter
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = False
      object RLSystemInfo1: TRLSystemInfo
        Left = 628
        Top = 11
        Width = 87
        Height = 16
        Alignment = taRightJustify
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = False
        Borders.DrawRight = False
        Borders.DrawBottom = False
        Info = itPageNumber
        Text = ''
      end
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 123
      Width = 718
      Height = 30
      BandType = btSummary
      object RLSystemInfo2: TRLSystemInfo
        Left = 123
        Top = 8
        Width = 79
        Height = 16
        Alignment = taRightJustify
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = False
        Borders.DrawRight = False
        Borders.DrawBottom = False
        Info = itDetailCount
        Text = ''
      end
      object RLLabel4: TRLLabel
        Left = 16
        Top = 10
        Width = 109
        Height = 16
        Caption = 'Total de Registros'
      end
    end
  end
  object qrDados: TFDQuery
    Connection = dmDados.fdConn
    SQL.Strings = (
      'select * from UF')
    Left = 680
    Top = 344
    object qrDadosID: TStringField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      FixedChar = True
      Size = 2
    end
    object qrDadosNOME: TStringField
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 100
    end
  end
  object dsDados: TDataSource
    DataSet = qrDados
    Left = 680
    Top = 400
  end
end
