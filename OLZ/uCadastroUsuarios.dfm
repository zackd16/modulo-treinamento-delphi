inherited fmCadastroUsuarios: TfmCadastroUsuarios
  Caption = 'fmCadastroUsuarios'
  ClientHeight = 426
  ClientWidth = 666
  ExplicitWidth = 682
  ExplicitHeight = 465
  PixelsPerInch = 96
  TextHeight = 13
  inherited pcPrincipal: TPageControl
    Width = 666
    Height = 426
    ExplicitWidth = 666
    ExplicitHeight = 426
    inherited tsDados: TTabSheet
      ExplicitWidth = 658
      ExplicitHeight = 398
      object Label1: TLabel [0]
        Left = 72
        Top = 112
        Width = 11
        Height = 13
        Caption = 'ID'
      end
      object Label2: TLabel [1]
        Left = 72
        Top = 160
        Width = 27
        Height = 13
        Caption = 'Nome'
      end
      object Label3: TLabel [2]
        Left = 72
        Top = 208
        Width = 24
        Height = 13
        Caption = 'Email'
      end
      object Label4: TLabel [3]
        Left = 72
        Top = 256
        Width = 30
        Height = 13
        Caption = 'Senha'
      end
      object Label5: TLabel [4]
        Left = 72
        Top = 304
        Width = 42
        Height = 13
        Caption = 'Telefone'
      end
      object Label6: TLabel [5]
        Left = 72
        Top = 352
        Width = 33
        Height = 13
        Caption = 'Cidade'
      end
      object Label7: TLabel [6]
        Left = 72
        Top = 400
        Width = 13
        Height = 13
        Caption = 'UF'
      end
      inherited pnButtonsDados: TPanel
        Left = 526
        Height = 398
        ExplicitLeft = 526
        ExplicitHeight = 398
        inherited nvDados: TDBNavigator
          Top = 369
          Hints.Strings = ()
          ExplicitTop = 369
        end
      end
      inherited grDados: TDBGrid
        Width = 526
        Height = 398
      end
    end
    inherited tsEdits: TTabSheet
      ExplicitWidth = 658
      ExplicitHeight = 398
      object Label8: TLabel [0]
        Left = 18
        Top = 17
        Width = 11
        Height = 13
        Caption = 'ID'
        FocusControl = edId
      end
      object Label9: TLabel [1]
        Left = 16
        Top = 63
        Width = 27
        Height = 13
        Caption = 'Nome'
        FocusControl = edNome
      end
      object Label10: TLabel [2]
        Left = 16
        Top = 103
        Width = 24
        Height = 13
        Caption = 'Email'
        FocusControl = edEmail
      end
      object Label11: TLabel [3]
        Left = 16
        Top = 189
        Width = 42
        Height = 13
        Caption = 'Telefone'
        FocusControl = edTelefone
      end
      object Label12: TLabel [4]
        Left = 16
        Top = 229
        Width = 33
        Height = 13
        Caption = 'Cidade'
        FocusControl = edCidade
      end
      object Label14: TLabel [5]
        Left = 16
        Top = 146
        Width = 30
        Height = 13
        Caption = 'Senha'
        FocusControl = edSenha
      end
      object Label13: TLabel [6]
        Left = 16
        Top = 269
        Width = 13
        Height = 13
        Caption = 'UF'
      end
      inherited pnButtonsEdits: TPanel
        Left = 526
        Height = 398
        ExplicitLeft = 526
        ExplicitHeight = 398
      end
      object edId: TDBEdit
        Left = 16
        Top = 36
        Width = 30
        Height = 21
        DataField = 'ID'
        DataSource = dsDados
        TabOrder = 1
      end
      object edNome: TDBEdit
        Left = 16
        Top = 79
        Width = 354
        Height = 21
        DataField = 'Nome'
        DataSource = dsDados
        TabOrder = 2
      end
      object edEmail: TDBEdit
        Left = 16
        Top = 119
        Width = 354
        Height = 21
        DataField = 'Email'
        DataSource = dsDados
        TabOrder = 3
      end
      object edTelefone: TDBEdit
        Left = 16
        Top = 205
        Width = 154
        Height = 21
        DataField = 'Telefone'
        DataSource = dsDados
        TabOrder = 5
      end
      object edCidade: TDBEdit
        Left = 16
        Top = 245
        Width = 154
        Height = 21
        DataField = 'Cidade'
        DataSource = dsDados
        TabOrder = 6
      end
      object edSenha: TDBEdit
        Left = 16
        Top = 162
        Width = 354
        Height = 21
        DataField = 'Senha'
        DataSource = dsDados
        PasswordChar = '*'
        TabOrder = 4
      end
      object lkUF: TDBLookupComboBox
        Left = 16
        Top = 288
        Width = 152
        Height = 21
        DataField = 'UF'
        DataSource = dsDados
        KeyField = 'id'
        ListField = 'id'
        ListSource = dsUf
        TabOrder = 7
      end
    end
  end
  inherited qrDados: TFDQuery
    SQL.Strings = (
      'select * from Usuario')
    Left = 552
    Top = 264
    object qrDadosID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      Required = True
    end
    object qrDadosNome: TStringField
      FieldName = 'Nome'
      Origin = 'Nome'
      Required = True
      Size = 50
    end
    object qrDadosEmail: TStringField
      FieldName = 'Email'
      Origin = 'Email'
      Required = True
      Size = 50
    end
    object qrDadosSenha: TStringField
      FieldName = 'Senha'
      Origin = 'Senha'
      Required = True
      Visible = False
      Size = 50
    end
    object qrDadosTelefone: TStringField
      FieldName = 'Telefone'
      Origin = 'Telefone'
      Required = True
      EditMask = '(99)99999-9999;0; '
      Size = 11
    end
    object qrDadosCidade: TStringField
      FieldName = 'Cidade'
      Origin = 'Cidade'
      Required = True
      Size = 50
    end
    object qrDadosUF: TStringField
      FieldName = 'UF'
      Origin = 'UF'
      Required = True
      FixedChar = True
      Size = 2
    end
  end
  inherited dsDados: TDataSource
    Left = 600
    Top = 264
  end
  object qrUf: TFDQuery
    Connection = dmDados.fdConn
    SQL.Strings = (
      'select id from uf')
    Left = 552
    Top = 200
    object qrUfid: TStringField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Visible = False
      FixedChar = True
      Size = 2
    end
  end
  object dsUf: TDataSource
    DataSet = qrUf
    Left = 596
    Top = 200
  end
end
