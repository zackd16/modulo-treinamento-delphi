unit uCadastroProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uCadastroBase, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.Grids, Vcl.DBGrids, Vcl.DBCtrls, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.Mask;

type
  TfmCadastroProdutos = class(TfmCadastroBase)
    Label1: TLabel;
    edId: TDBEdit;
    Label2: TLabel;
    edTitulo: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    imImagem: TDBImage;
    edPreco: TDBEdit;
    lkUf: TDBLookupComboBox;
    qrUf: TFDQuery;
    dsUf: TDataSource;
    btCarregar: TButton;
    btLimpar: TButton;
    odImagem: TOpenDialog;
    qrCategorias: TFDQuery;
    dsCategorias: TDataSource;
    qrCategoriasid: TFDAutoIncField;
    qrCategoriasnome: TStringField;
    Label5: TLabel;
    edCategorias: TDBEdit;
    lkCategorias: TDBLookupComboBox;
    mmDescricao: TDBMemo;
    qrDadosID: TFDAutoIncField;
    qrDadosDESCRICAO: TStringField;
    qrDadosPRECO: TCurrencyField;
    qrDadosUF_ID: TStringField;
    qrDadosCATEGORIA_ID: TIntegerField;
    qrDadosUSUARIO_ID: TIntegerField;
    qrDadosIMAGEM: TBlobField;
    qrDadosTITULO: TStringField;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure qrDadosBeforePost(DataSet: TDataSet);
    procedure btCarregarClick(Sender: TObject);
    procedure btLimparClick(Sender: TObject);
    procedure btInserirClick(Sender: TObject);
    procedure btEditarClick(Sender: TObject);
  private
    procedure SalvarImagem(Caminho:string);
    protected
    function ValidarObrigatorios: boolean; override;
  public
    { Public declarations }
  end;

var
  fmCadastroProdutos: TfmCadastroProdutos;

implementation
uses
  udmDados, jpeg;
  {$R *.dfm}

procedure TfmCadastroProdutos.btCarregarClick(Sender: TObject);
begin
  inherited;
  if odImagem.Execute then
    SalvarImagem(odImagem.FileName);
end;

procedure TfmCadastroProdutos.btEditarClick(Sender: TObject);
begin
  inherited;
  edTitulo.SetFocus;
end;

procedure TfmCadastroProdutos.btInserirClick(Sender: TObject);
begin
  inherited;
  edTitulo.SetFocus;
end;

procedure TfmCadastroProdutos.btLimparClick(Sender: TObject);
begin
  inherited;
  imImagem.Picture.Bitmap := nil;
end;

procedure TfmCadastroProdutos.FormClose(Sender: TObject;
var Action: TCloseAction);
begin
  inherited;
  fmCadastroProdutos := nil;
end;

procedure TfmCadastroProdutos.FormShow(Sender: TObject);
begin
  inherited;
  qrDados.Open;
  qrUf.Open;
  qrCategorias.Open;
end;

procedure TfmCadastroProdutos.qrDadosBeforePost(DataSet: TDataSet);
begin
  inherited;
  qrDadosUSUARIO_ID.AsInteger := 1;
//  qrDadosDATA_VENDA.AsInteger := 1;
//  qrDadosDATA_CADASTRO.AsInteger := 1;
end;

procedure TfmCadastroProdutos.SalvarImagem(Caminho: string);
var
  jpg: TJPEGImage;
  bmp: TBitmap;
begin
  jpg:= TJPEGImage.Create;
  try
    jpg.LoadFromFile(Caminho);
    bmp:=TBitmap.Create;
    try
      bmp.Assign(jpg);
      imImagem.Picture.Bitmap.Assign(bmp);
    finally
      bmp.Free;
    end;
  finally
    jpg.Free;
  end;
end;

function TfmCadastroProdutos.ValidarObrigatorios: boolean;
begin
  if Trim(edTitulo.Text) = '' then
  begin
    ShowMessage('Informe o titulo');
    edTitulo.SetFocus;
    Exit(false);
  end;

  if Trim(mmDescricao.Text) = '' then
  begin
    ShowMessage('Informe a descri��o');
    mmDescricao.SetFocus;
    Exit(false);
  end;

  if Trim(edPreco.Text) = '' then
  begin
    ShowMessage('Informe o pre�o');
    edPreco.SetFocus;
    Exit(false);
  end;

  if Trim(lkUf.Text) = '' then
  begin
    ShowMessage('Informe a unidade federativa');
    lkUf.SetFocus;
    Exit(false);
  end;

  if Trim(lkCategorias.Text) = '' then
  begin
    ShowMessage('Informe a categoria do produto');
    lkCategorias.SetFocus;
    Exit(false);
  end;

  Result := inherited ValidarObrigatorios;
end;
end.
