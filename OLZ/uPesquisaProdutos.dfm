object fmPesquisaProdutos: TfmPesquisaProdutos
  Left = 0
  Top = 0
  BiDiMode = bdLeftToRight
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Pesquisa de Produtos'
  ClientHeight = 492
  ClientWidth = 820
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 536
    Top = 11
    Width = 28
    Height = 18
    Caption = 'Em:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object pnPesquisa: TPanel
    Left = 0
    Top = 0
    Width = 820
    Height = 73
    Align = alTop
    BevelOuter = bvNone
    Color = clTeal
    ParentBackground = False
    TabOrder = 0
    ExplicitLeft = 24
    ExplicitTop = -6
    object Label1: TLabel
      Left = 16
      Top = 11
      Width = 53
      Height = 18
      Caption = 'Buscar:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 344
      Top = 11
      Width = 28
      Height = 18
      Caption = 'Em:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 536
      Top = 11
      Width = 29
      Height = 18
      Caption = 'Por:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object edBusca: TEdit
      Left = 16
      Top = 35
      Width = 305
      Height = 21
      TabOrder = 0
      Text = 'edBusca'
      OnKeyDown = edBuscaKeyDown
    end
    object cbFiltro: TComboBox
      Left = 344
      Top = 35
      Width = 161
      Height = 21
      ItemIndex = 0
      TabOrder = 1
      Text = 'Produto'
      OnKeyDown = cbFiltroKeyDown
      Items.Strings = (
        'Produto'
        'Categoria'
        'UF')
    end
  end
  object pnInfo: TPanel
    Left = 495
    Top = 73
    Width = 325
    Height = 419
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = 328
    ExplicitHeight = 410
    object imImagem: TDBImage
      Left = 24
      Top = 17
      Width = 281
      Height = 256
      DataField = 'IMAGEM'
      DataSource = dsDados
      Stretch = True
      TabOrder = 0
    end
    object mmDescricao: TDBMemo
      Left = 24
      Top = 279
      Width = 281
      Height = 137
      DataField = 'DESCRICAO'
      DataSource = dsDados
      TabOrder = 1
    end
  end
  object grDados: TDBGrid
    Left = 0
    Top = 73
    Width = 495
    Height = 419
    Align = alClient
    DataSource = dsDados
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object cbOrdem: TComboBox
    Left = 536
    Top = 35
    Width = 161
    Height = 21
    ItemIndex = 0
    TabOrder = 3
    Text = 'T'#237'tulo A-Z'
    OnKeyDown = cbFiltroKeyDown
    Items.Strings = (
      'T'#237'tulo A-Z'
      'T'#237'tulo Z-A'
      'Pre'#231'o crescente'
      'Pre'#231'o decrescente')
  end
  object qrDados: TFDQuery
    Connection = dmDados.fdConn
    SQL.Strings = (
      'select '
      #9'prod.ID,'
      #9'prod.TITULO,'
      #9'prod.DESCRICAO,'
      '        prod.PRECO,'
      #9'prod.IMAGEM,'
      #9'cate.NOME as Categoria,'
      #9'UF.ID as UF'
      'from Produtos as prod'
      'inner join Categorias as cate on cate.ID = prod.CATEGORIA_ID'
      'inner join UF on UF.ID = prod.UF_ID')
    Left = 234
    Top = 441
    object qrDadosID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
      Visible = False
    end
    object qrDadosTITULO: TStringField
      FieldName = 'TITULO'
      Origin = 'TITULO'
      Required = True
      Size = 100
    end
    object qrDadosDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Visible = False
      Size = 255
    end
    object qrDadosIMAGEM: TBlobField
      FieldName = 'IMAGEM'
      Origin = 'IMAGEM'
      Required = True
      Visible = False
    end
    object qrDadosCategoria: TStringField
      FieldName = 'Categoria'
      Origin = 'Categoria'
      Required = True
      Visible = False
      Size = 50
    end
    object qrDadosUF: TStringField
      FieldName = 'UF'
      Origin = 'UF'
      Required = True
      Visible = False
      FixedChar = True
      Size = 2
    end
    object qrDadosPRECO: TCurrencyField
      FieldName = 'PRECO'
      Origin = 'PRECO'
      Required = True
    end
  end
  object dsDados: TDataSource
    DataSet = qrDados
    Left = 282
    Top = 441
  end
end
