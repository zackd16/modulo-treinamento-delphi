unit uListagemProdutosPorCategoria;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, RLReport, Vcl.StdCtrls,
  Vcl.DBCtrls, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfmListagemProdutosPorCategoria = class(TForm)
    rpPrincipal: TRLReport;
    RLBand1: TRLBand;
    RLLabel1: TRLLabel;
    RLGroup1: TRLGroup;
    qrDados: TFDQuery;
    dsDados: TDataSource;
    RLBand2: TRLBand;
    RLLabel3: TRLLabel;
    RLDBText2: TRLDBText;
    RLLabel2: TRLLabel;
    RLDBText1: TRLDBText;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLBand3: TRLBand;
    RLDBText3: TRLDBText;
    RLDBText5: TRLDBText;
    RLBand4: TRLBand;
    RLSystemInfo1: TRLSystemInfo;
    procedure rpPrincipalBeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmListagemProdutosPorCategoria: TfmListagemProdutosPorCategoria;

implementation

{$R *.dfm}

uses
  udmDados;

procedure TfmListagemProdutosPorCategoria.rpPrincipalBeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  qrDados.Open;
end;

end.
