unit uCadastroBase;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.DBCtrls;

type
  TfmCadastroBase = class(TForm)
    pcPrincipal: TPageControl;
    tsDados: TTabSheet;
    tsEdits: TTabSheet;
    pnButtonsDados: TPanel;
    btInserir: TButton;
    btEditar: TButton;
    btExcluir: TButton;
    grDados: TDBGrid;
    pnButtonsEdits: TPanel;
    btSalvar: TButton;
    btCancelar: TButton;
    qrDados: TFDQuery;
    dsDados: TDataSource;
    nvDados: TDBNavigator;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btInserirClick(Sender: TObject);
    procedure btEditarClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
  private
    procedure EsconderAbas;
  protected
    function ValidarObrigatorios: boolean; virtual;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCadastroBase: TfmCadastroBase;

implementation

{$R *.dfm}

uses udmDados;

procedure TfmCadastroBase.btCancelarClick(Sender: TObject);
begin
  qrDados.Cancel;
  pcPrincipal.ActivePage:= tsDados;
end;

procedure TfmCadastroBase.btEditarClick(Sender: TObject);
begin
  pcPrincipal.ActivePage:= tsEdits;
  qrDados.Edit;
end;

procedure TfmCadastroBase.btExcluirClick(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o do Registro?', 'OLZ', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
    qrDados.Delete;
end;

procedure TfmCadastroBase.btInserirClick(Sender: TObject);
begin
  pcPrincipal.ActivePage:= tsEdits;
  qrDados.Append;
end;

procedure TfmCadastroBase.btSalvarClick(Sender: TObject);
begin
  if ValidarObrigatorios then
  begin
    qrDados.Post;
    pcPrincipal.ActivePage:= tsDados;
  end;
end;

procedure TfmCadastroBase.EsconderAbas;
var
  I: Integer;
begin
  for I := 0 to pcPrincipal.PageCount - 1 do
    pcPrincipal.Pages[I].tabVisible := false;

  pcPrincipal.ActivePage:= tsDados;
end;

procedure TfmCadastroBase.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfmCadastroBase.FormShow(Sender: TObject);
begin
  EsconderAbas;
  qrDados.Open();
end;

function TfmCadastroBase.ValidarObrigatorios: boolean;
begin
  Result := true;
end;

end.
