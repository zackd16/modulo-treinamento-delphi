unit uPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.Menus, Vcl.ExtCtrls;

type
  TfmPrincipal = class(TForm)
    mmPrincipal: TMainMenu;
    Cadastros1: TMenuItem;
    Categorias1: TMenuItem;
    Usurios1: TMenuItem;
    Produtos1: TMenuItem;
    Pesquisa1: TMenuItem;
    StatusBar1: TStatusBar;
    Image1: TImage;
    procedure Categorias1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPrincipal: TfmPrincipal;

implementation

{$R *.dfm}

uses uCadastroCategorias;

procedure TfmPrincipal.Categorias1Click(Sender: TObject);
begin
    if fmCadastroCategorias = nil then
    fmCadastroCategorias := TfmCadastroCategorias.Create(Self);
    fmCadastroCategorias.Show;
end;

end.
