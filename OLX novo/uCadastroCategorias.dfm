inherited fmCadastroCategorias: TfmCadastroCategorias
  Caption = 'Cadastro de Categorias'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pcPrincipal: TPageControl
    ActivePage = tsEdits
    inherited tsDados: TTabSheet
      inherited pnButtonsDados: TPanel
        ExplicitLeft = 537
      end
    end
  end
  inherited qrDados: TFDQuery
    SQL.Strings = (
      'select * from categorias')
    object qrDadosID: TFDAutoIncField
      DisplayLabel = 'Id'
      DisplayWidth = 10
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object qrDadosNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Origin = 'NOME'
      Required = True
      Size = 50
    end
  end
end
