object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'sqlserver'
  ClientHeight = 291
  ClientWidth = 510
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object grClientes: TDBGrid
    Left = 0
    Top = 0
    Width = 510
    Height = 291
    Align = alClient
    DataSource = dsClientes
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object FDConn: TFDConnection
    Params.Strings = (
      'Database=FIREDAC'
      'User_Name=sa'
      'Server=NJTREI001'
      'DriverID=MSSQL')
    LoginPrompt = False
    Left = 32
    Top = 48
  end
  object qrClientes: TFDQuery
    Connection = FDConn
    SQL.Strings = (
      'select * from Clientes')
    Left = 32
    Top = 96
  end
  object dsClientes: TDataSource
    DataSet = qrClientes
    Left = 32
    Top = 152
  end
end
