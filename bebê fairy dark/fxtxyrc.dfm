object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'sqlserver'
  ClientHeight = 474
  ClientWidth = 689
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 544
    Top = 416
    Width = 105
    Height = 105
  end
  object Image2: TImage
    Left = 152
    Top = 184
    Width = 105
    Height = 105
  end
  object grClientes: TDBGrid
    Left = 0
    Top = 0
    Width = 689
    Height = 474
    Align = alClient
    DataSource = dsClientes
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Width = 37
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SOBRENOME'
        Visible = True
      end>
  end
  object FDConn: TFDConnection
    Params.Strings = (
      'Database=FIREDAC'
      'User_Name=sa'
      'Server=NJTREI001'
      'DriverID=MSSQL')
    Connected = True
    LoginPrompt = False
    Left = 32
    Top = 280
  end
  object qrClientes: TFDQuery
    Active = True
    Connection = FDConn
    SQL.Strings = (
      'select * from Clientes')
    Left = 32
    Top = 320
  end
  object dsClientes: TDataSource
    DataSet = qrClientes
    Left = 32
    Top = 368
  end
end
