unit UPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TfmCalc = class(TForm)
    btSete: TButton;
    btOito: TButton;
    btNove: TButton;
    btQuatro: TButton;
    btCinco: TButton;
    btSeis: TButton;
    btUm: TButton;
    btDois: TButton;
    btTres: TButton;
    btResultado: TButton;
    btZero: TButton;
    btPonto: TButton;
    btApagar: TButton;
    btSoma: TButton;
    btSubtracao: TButton;
    btMultiplicacao: TButton;
    btDivisao: TButton;
    edResultado: TEdit;
    procedure btDoisClick(Sender: TObject);
    procedure btTresClick(Sender: TObject);
    procedure btUmClick(Sender: TObject);
    procedure btCincoClick(Sender: TObject);
    procedure btSeisClick(Sender: TObject);
    procedure btSeteClick(Sender: TObject);
    procedure btOitoClick(Sender: TObject);
    procedure btNoveClick(Sender: TObject);
    procedure btQuatroClick(Sender: TObject);
    procedure btSomaClick(Sender: TObject);
    procedure btResultadoClick(Sender: TObject);
    procedure btApagarClick(Sender: TObject);
    procedure btSubtracaoClick(Sender: TObject);
    procedure btMultiplicacaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCalc: TfmCalc;
  valor1: real;
  valor2: real;
  funcao: integer;

implementation

{$R *.dfm}

procedure TfmCalc.btUmClick(Sender: TObject);
begin
edResultado.Text:= edResultado.Text + btUm.Caption;
end;

procedure TfmCalc.btDoisClick(Sender: TObject);
begin
edResultado.Text:= edResultado.Text + btDois.Caption;
end;


procedure TfmCalc.btTresClick(Sender: TObject);
begin
edResultado.Text:= edResultado.Text + btTres.Caption;
end;

procedure TfmCalc.btQuatroClick(Sender: TObject);
begin
edResultado.Text:= edResultado.Text + btQuatro.Caption;
end;



procedure TfmCalc.btCincoClick(Sender: TObject);
begin
edResultado.Text:= edResultado.Text + btCinco.Caption;
end;

procedure TfmCalc.btSeisClick(Sender: TObject);
begin
edResultado.Text:= edResultado.Text + btSeis.Caption;
end;


procedure TfmCalc.btSeteClick(Sender: TObject);
begin
edResultado.Text:= edResultado.Text + btSete.Caption;
end;


procedure TfmCalc.btOitoClick(Sender: TObject);
begin
edResultado.Text:= edResultado.Text + btOito.Caption;
end;

procedure TfmCalc.btNoveClick(Sender: TObject);
begin
edResultado.Text:= edResultado.Text + btNove.Caption;
end;



procedure TfmCalc.btSomaClick(Sender: TObject);
begin
  valor1 := StrToFloat(edResultado.Text);
  edResultado.Text := '';
  funcao := 1;
end;


procedure TfmCalc.btSubtracaoClick(Sender: TObject);
begin
  valor1 := StrToFloat(edResultado.Text);
  edResultado.Text := '';
  funcao := 2;
end;

procedure TfmCalc.btMultiplicacaoClick(Sender: TObject);
begin
  valor1 := StrToFloat(edResultado.Text);
  edResultado.Text := '';
  funcao := 3;
end;


procedure TfmCalc.btResultadoClick(Sender: TObject);
var resultado: real;

begin
    valor2:=StrToFloat(edResultado.Text);
  case (funcao) of
  1:
  begin
    resultado:=valor1+valor2;
    edResultado.text:=FloatToStr(resultado);
  end;
  2:
  begin
    resultado:=valor1-valor2;
    edResultado.text:=FloatToStr(resultado);
  end;
  3:
  begin
    resultado:=valor1*valor2;
    edResultado.text:=FloatToStr(resultado);
  end;
  4:
  begin
    resultado:=valor1/valor2;
    edResultado.text:=FloatToStr(resultado);
  end;
end;
end;

procedure TfmCalc.btApagarClick(Sender: TObject);
begin
  edResultado.Text := '';
end;

end.
